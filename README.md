# Job shadowing project: charm jet tagging with excited charged D mesons

This gitlab project contains a self-contained data-analysis project to be tackled during two days.

To obtain the content locally, clone the project:

```bash
git clone https://gitlab.cern.ch/pgadow/job-shadowing.git
```

Make sure that you have at least 100MB free disk space because in addition to the python-based analysis software you will also download one dataset file.


## Data analysis

The data analysis is foreseen to be done in a jupyter notebook.
If your computer is not yet readily set up, please install a modern python version and the `pip` package manager so you can

```bash
pip install jupyter
```

There is also a nice desktop-app available: [JupyterLab Desktop App now available!](https://blog.jupyter.org/jupyterlab-desktop-app-now-available-b8b661b17e9a)

Once everythin is set up, open `reco-plotting.ipynb` with your jupyter instance.
